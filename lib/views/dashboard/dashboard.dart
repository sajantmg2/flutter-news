import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_news/helper/dart.dart';
import 'package:flutter_news/helper/news.dart';
import 'package:flutter_news/models/ArticleModel.dart';
import 'package:flutter_news/models/category_model.dart';

import 'article_view.dart';
import 'category.dart';


class Dashboard extends StatefulWidget {

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  bool _loading = true;
  List<CategoryModel> categoryList = new List.empty(growable: false);
  List<ArticleModel> articleList = new List.empty(growable: false);

  @override
  void initState() {
    super.initState();
    categoryList = getCategories();
    getNews();
  }

  getNews() async {
    News newsClass = News();
    await newsClass.getNews();
    articleList = newsClass.news;
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  _loading
          ? Center(
        child: Container(
          child: CircularProgressIndicator(),
        ),
      )
          : SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              ///Categories
              Container(
                height: 70,
                child: ListView.builder(
                    itemCount: categoryList.length,
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return CategoryTile(
                        categoryImageUrl:
                        categoryList[index].categoryImageUrl,
                        categoryName: categoryList[index].categoryName,
                      );
                    }),
              ),

              ///TopNews
              Container(
                padding: EdgeInsets.only(top: 16),
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: articleList.length,
                    itemBuilder: (context, index) {
                      return BlogTile(
                        newsImageUrl: articleList[index].articleImageUrl,
                        newsTitle: articleList[index].articleTitle,
                        newsDescription: articleList[index].articleDesc,
                        newsUrl: articleList[index].urlToNews,
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

}

class CategoryTile extends StatelessWidget {
  final categoryImageUrl, categoryName;

  CategoryTile({this.categoryImageUrl, this.categoryName});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
            builder: (context) => Category(
              categoryStringFromHome: categoryName,
            )
        ));
      },
      child: Container(
        margin: EdgeInsets.only(right: 15),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: CachedNetworkImage(
                imageUrl: categoryImageUrl,
                width: 120,
                height: 60,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              alignment: Alignment.center,
              width: 120,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.black38),
              child: Text(
                categoryName,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class BlogTile extends StatelessWidget {
  final String newsImageUrl, newsTitle, newsDescription, newsUrl;

  BlogTile(
      {@required this.newsImageUrl,
        @required this.newsTitle,
        @required this.newsDescription,
        @required this.newsUrl});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ArticleView(
          newsUrl: newsUrl,
        )));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        child: Column(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.network(newsImageUrl)),
            SizedBox(
              height: 8,
            ),
            Text(
              newsTitle,
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              newsDescription,
              style: TextStyle(color: Colors.grey),
            ),
          ],
        ),
      ),
    );
  }
}


