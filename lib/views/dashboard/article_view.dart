import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ArticleView extends StatefulWidget {
  final String newsUrl;
  const ArticleView({Key key, this.newsUrl}) : super(key: key);

  @override
  _ArticleViewState createState() => _ArticleViewState();
}

class _ArticleViewState extends State<ArticleView> {

final Completer<WebViewController> _completer = Completer<WebViewController>();
num position = 1 ;

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();

}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          //"समाचार",
          widget.newsUrl,
          style: TextStyle(color: Colors.lightBlue  , fontWeight: FontWeight.bold,
          fontSize: 14),
        ),
        elevation: 0,
      ),
      body: IndexedStack(
        //height: MediaQuery.of(context).size.height,
        //width: MediaQuery.of(context).size.width,
        children : [
          WebView(
            initialUrl: widget.newsUrl,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: ((WebViewController webViewController){
              _completer.complete(webViewController);
            }),
            onPageStarted: (value){setState(() {
              position = 1;
            });},
            onPageFinished: (value){setState(() {
              position = 0;
            });},

          ),
          Container(
            child: Center(
                child: CircularProgressIndicator()),
          ),
        ]


      ),
    );
  }
}
