import 'package:flutter/material.dart';
import 'package:flutter_news/helper/news.dart';
import 'package:flutter_news/models/ArticleModel.dart';

import 'dashboard.dart';


class Category extends StatefulWidget {
  final String categoryStringFromHome;

  const Category({Key key, @required this.categoryStringFromHome}) : super(key: key);

  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  List<ArticleModel> articleList = new List.empty(growable: false);
  bool _loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getNews();
  }

  getNews() async {
    CategoryNews categoryNews = CategoryNews();
    await categoryNews.getNews(widget.categoryStringFromHome);
    articleList = categoryNews.news;
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(widget.categoryStringFromHome),
              Text(
                //"समाचार",
                " News",
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
              )
            ],
          ),
          elevation: 0,
        ),
        body: _loading
            ? Center(
                child: Container(
                  child: CircularProgressIndicator(),
                ),
              )
            : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(top: 16),
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: articleList.length,
                    itemBuilder: (context, index) {
                      return BlogTile(
                        newsImageUrl: articleList[index].articleImageUrl,
                        newsTitle: articleList[index].articleTitle,
                        newsDescription: articleList[index].articleDesc,
                        newsUrl: articleList[index].urlToNews,
                      );
                    }),
              ),
            ));
  }
}

/*class BlogTile extends StatelessWidget {
  final String newsImageUrl, newsTitle, newsDescription, newsUrl;

  BlogTile(
      {@required this.newsImageUrl,
        @required this.newsTitle,
        @required this.newsDescription,
        @required this.newsUrl});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ArticleView(
          newsUrl: newsUrl,
        )));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 16),
        child: Column(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.network(newsImageUrl)),
            SizedBox(
              height: 8,
            ),
            Text(
              newsTitle,
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              newsDescription,
              style: TextStyle(color: Colors.grey),
            ),
          ],
        ),
      ),
    );
  }
}*/
