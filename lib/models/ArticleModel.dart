class ArticleModel {
  String author, articleImageUrl, articleTitle, articleDesc, urlToNews, content;

  ArticleModel(
      {this.author,
      this.articleDesc,
      this.articleImageUrl,
      this.articleTitle,
      this.content,
      this.urlToNews});
}
